// Code generated by protoc-gen-go. DO NOT EDIT.
// source: aiot_business.proto

package aiot

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// 请求类型
type RequestType int32

const (
	// 同步请求 默认
	RequestType_sync RequestType = 0
	// 异步请求
	RequestType_Async RequestType = 1
)

var RequestType_name = map[int32]string{
	0: "sync",
	1: "Async",
}

var RequestType_value = map[string]int32{
	"sync":  0,
	"Async": 1,
}

func (x RequestType) String() string {
	return proto.EnumName(RequestType_name, int32(x))
}

func (RequestType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_29eca6d4b33e7fd8, []int{0}
}

// 请求方式
type RequestMethod int32

const (
	// 默认
	RequestMethod_MethodDefault RequestMethod = 0
	// post请求
	RequestMethod_Post RequestMethod = 1
	// get请求
	RequestMethod_Get RequestMethod = 2
	// put请求
	RequestMethod_Put RequestMethod = 3
	// delete请求
	RequestMethod_Delete RequestMethod = 4
)

var RequestMethod_name = map[int32]string{
	0: "MethodDefault",
	1: "Post",
	2: "Get",
	3: "Put",
	4: "Delete",
}

var RequestMethod_value = map[string]int32{
	"MethodDefault": 0,
	"Post":          1,
	"Get":           2,
	"Put":           3,
	"Delete":        4,
}

func (x RequestMethod) String() string {
	return proto.EnumName(RequestMethod_name, int32(x))
}

func (RequestMethod) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_29eca6d4b33e7fd8, []int{1}
}

// 请求header
type RequestContentType int32

const (
	RequestContentType_ContentTypeDefault            RequestContentType = 0
	RequestContentType_ApplicationJson               RequestContentType = 1
	RequestContentType_ApplicationXWwwFormUrlencoded RequestContentType = 2
	RequestContentType_MultipartFormData             RequestContentType = 3
	RequestContentType_ApplicationXml                RequestContentType = 4
)

var RequestContentType_name = map[int32]string{
	0: "ContentTypeDefault",
	1: "ApplicationJson",
	2: "ApplicationXWwwFormUrlencoded",
	3: "MultipartFormData",
	4: "ApplicationXml",
}

var RequestContentType_value = map[string]int32{
	"ContentTypeDefault":            0,
	"ApplicationJson":               1,
	"ApplicationXWwwFormUrlencoded": 2,
	"MultipartFormData":             3,
	"ApplicationXml":                4,
}

func (x RequestContentType) String() string {
	return proto.EnumName(RequestContentType_name, int32(x))
}

func (RequestContentType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_29eca6d4b33e7fd8, []int{2}
}

// 设备列表请求参数
type BusinessReq struct {
	// 请求的topic url
	Topic string `protobuf:"bytes,1,opt,name=topic,proto3" json:"topic,omitempty"`
	// 请求的同步/异步类型
	SyncType RequestType `protobuf:"varint,4,opt,name=syncType,proto3,enum=aiot.RequestType" json:"syncType,omitempty"`
	// method
	Method RequestMethod `protobuf:"varint,5,opt,name=method,proto3,enum=aiot.RequestMethod" json:"method,omitempty"`
	// content-type
	ContentType RequestContentType `protobuf:"varint,6,opt,name=contentType,proto3,enum=aiot.RequestContentType" json:"contentType,omitempty"`
	// 请求业务参数
	Req                  []byte   `protobuf:"bytes,7,opt,name=req,proto3" json:"req,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BusinessReq) Reset()         { *m = BusinessReq{} }
func (m *BusinessReq) String() string { return proto.CompactTextString(m) }
func (*BusinessReq) ProtoMessage()    {}
func (*BusinessReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_29eca6d4b33e7fd8, []int{0}
}

func (m *BusinessReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BusinessReq.Unmarshal(m, b)
}
func (m *BusinessReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BusinessReq.Marshal(b, m, deterministic)
}
func (m *BusinessReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BusinessReq.Merge(m, src)
}
func (m *BusinessReq) XXX_Size() int {
	return xxx_messageInfo_BusinessReq.Size(m)
}
func (m *BusinessReq) XXX_DiscardUnknown() {
	xxx_messageInfo_BusinessReq.DiscardUnknown(m)
}

var xxx_messageInfo_BusinessReq proto.InternalMessageInfo

func (m *BusinessReq) GetTopic() string {
	if m != nil {
		return m.Topic
	}
	return ""
}

func (m *BusinessReq) GetSyncType() RequestType {
	if m != nil {
		return m.SyncType
	}
	return RequestType_sync
}

func (m *BusinessReq) GetMethod() RequestMethod {
	if m != nil {
		return m.Method
	}
	return RequestMethod_MethodDefault
}

func (m *BusinessReq) GetContentType() RequestContentType {
	if m != nil {
		return m.ContentType
	}
	return RequestContentType_ContentTypeDefault
}

func (m *BusinessReq) GetReq() []byte {
	if m != nil {
		return m.Req
	}
	return nil
}

// 响应的body体
type BusinessReply struct {
	// 返回的错误码
	Code int32 `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	// 错误信息
	Msg string `protobuf:"bytes,2,opt,name=msg,proto3" json:"msg,omitempty"`
	// 是否success
	Success bool `protobuf:"varint,3,opt,name=success,proto3" json:"success,omitempty"`
	// 返回结果
	Data                 []byte   `protobuf:"bytes,4,opt,name=data,proto3" json:"data,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BusinessReply) Reset()         { *m = BusinessReply{} }
func (m *BusinessReply) String() string { return proto.CompactTextString(m) }
func (*BusinessReply) ProtoMessage()    {}
func (*BusinessReply) Descriptor() ([]byte, []int) {
	return fileDescriptor_29eca6d4b33e7fd8, []int{1}
}

func (m *BusinessReply) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BusinessReply.Unmarshal(m, b)
}
func (m *BusinessReply) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BusinessReply.Marshal(b, m, deterministic)
}
func (m *BusinessReply) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BusinessReply.Merge(m, src)
}
func (m *BusinessReply) XXX_Size() int {
	return xxx_messageInfo_BusinessReply.Size(m)
}
func (m *BusinessReply) XXX_DiscardUnknown() {
	xxx_messageInfo_BusinessReply.DiscardUnknown(m)
}

var xxx_messageInfo_BusinessReply proto.InternalMessageInfo

func (m *BusinessReply) GetCode() int32 {
	if m != nil {
		return m.Code
	}
	return 0
}

func (m *BusinessReply) GetMsg() string {
	if m != nil {
		return m.Msg
	}
	return ""
}

func (m *BusinessReply) GetSuccess() bool {
	if m != nil {
		return m.Success
	}
	return false
}

func (m *BusinessReply) GetData() []byte {
	if m != nil {
		return m.Data
	}
	return nil
}

func init() {
	proto.RegisterEnum("aiot.RequestType", RequestType_name, RequestType_value)
	proto.RegisterEnum("aiot.RequestMethod", RequestMethod_name, RequestMethod_value)
	proto.RegisterEnum("aiot.RequestContentType", RequestContentType_name, RequestContentType_value)
	proto.RegisterType((*BusinessReq)(nil), "aiot.BusinessReq")
	proto.RegisterType((*BusinessReply)(nil), "aiot.BusinessReply")
}

func init() {
	proto.RegisterFile("aiot_business.proto", fileDescriptor_29eca6d4b33e7fd8)
}

var fileDescriptor_29eca6d4b33e7fd8 = []byte{
	// 412 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x64, 0x52, 0x4d, 0x8b, 0x13, 0x41,
	0x10, 0x4d, 0x67, 0x26, 0x1f, 0x5b, 0xd9, 0xac, 0x9d, 0x8a, 0x4a, 0x23, 0x08, 0x31, 0xa7, 0x10,
	0x31, 0x87, 0xdd, 0x9b, 0xb7, 0xac, 0x41, 0x61, 0x61, 0x61, 0x69, 0x15, 0xbd, 0xc9, 0x6c, 0xa7,
	0xd4, 0x81, 0xc9, 0x74, 0x67, 0xba, 0xc6, 0x25, 0xbf, 0xc2, 0x5f, 0xe6, 0x7f, 0x92, 0xee, 0x44,
	0x33, 0xd1, 0xdb, 0xab, 0x7a, 0xef, 0x35, 0xfd, 0x8a, 0x07, 0xe3, 0x2c, 0xb7, 0xfc, 0xe5, 0xbe,
	0xf6, 0x79, 0x49, 0xde, 0x2f, 0x5c, 0x65, 0xd9, 0x62, 0x1a, 0x96, 0xd3, 0x5f, 0x02, 0x06, 0xd7,
	0x07, 0x42, 0xd3, 0x16, 0x1f, 0x43, 0x87, 0xad, 0xcb, 0x8d, 0x12, 0x13, 0x31, 0x3b, 0xd3, 0xfb,
	0x01, 0x5f, 0x41, 0xdf, 0xef, 0x4a, 0xf3, 0x61, 0xe7, 0x48, 0xa5, 0x13, 0x31, 0xbb, 0xb8, 0x1c,
	0x2d, 0x82, 0x7d, 0xa1, 0x69, 0x5b, 0x93, 0xe7, 0x40, 0xe8, 0xbf, 0x12, 0x7c, 0x09, 0xdd, 0x0d,
	0xf1, 0x77, 0xbb, 0x56, 0x9d, 0x28, 0x1e, 0x9f, 0x88, 0x6f, 0x23, 0xa5, 0x0f, 0x12, 0x7c, 0x0d,
	0x03, 0x63, 0x4b, 0xa6, 0x32, 0xbe, 0xa2, 0xba, 0xd1, 0xa1, 0x4e, 0x1c, 0x6f, 0x8e, 0xbc, 0x6e,
	0x8a, 0x51, 0x42, 0x52, 0xd1, 0x56, 0xf5, 0x26, 0x62, 0x76, 0xae, 0x03, 0x9c, 0x1a, 0x18, 0x1e,
	0xe3, 0xb8, 0x62, 0x87, 0x08, 0xa9, 0xb1, 0x6b, 0x8a, 0x79, 0x3a, 0x3a, 0xe2, 0x60, 0xdb, 0xf8,
	0x6f, 0xaa, 0x1d, 0x23, 0x06, 0x88, 0x0a, 0x7a, 0xbe, 0x36, 0x86, 0xbc, 0x57, 0xc9, 0x44, 0xcc,
	0xfa, 0xfa, 0xcf, 0x18, 0xfc, 0xeb, 0x8c, 0xb3, 0x18, 0xfb, 0x5c, 0x47, 0x3c, 0x9f, 0xc2, 0xa0,
	0x11, 0x1c, 0xfb, 0x90, 0x86, 0xe8, 0xb2, 0x85, 0x67, 0xd0, 0x59, 0x46, 0x28, 0xe6, 0x37, 0x30,
	0x3c, 0xc9, 0x8b, 0x23, 0x18, 0xee, 0xd1, 0x8a, 0xbe, 0x66, 0x75, 0xc1, 0xb2, 0x15, 0x8c, 0x77,
	0xd6, 0xb3, 0x14, 0xd8, 0x83, 0xe4, 0x1d, 0xb1, 0x6c, 0x07, 0x70, 0x57, 0xb3, 0x4c, 0x10, 0xa0,
	0xbb, 0xa2, 0x82, 0x98, 0x64, 0x3a, 0xff, 0x29, 0x00, 0xff, 0x3f, 0x05, 0x3e, 0x05, 0x6c, 0x8c,
	0xc7, 0x67, 0xc7, 0xf0, 0x68, 0xe9, 0x5c, 0x91, 0x9b, 0x8c, 0x73, 0x5b, 0xde, 0x78, 0x5b, 0x4a,
	0x81, 0x2f, 0xe0, 0x79, 0x63, 0xf9, 0xf9, 0xd3, 0xc3, 0xc3, 0x5b, 0x5b, 0x6d, 0x3e, 0x56, 0x05,
	0x95, 0xe1, 0x26, 0x6b, 0xd9, 0xc6, 0x27, 0x30, 0xba, 0xad, 0x0b, 0xce, 0x5d, 0x56, 0x71, 0x20,
	0x57, 0x19, 0x67, 0x32, 0x41, 0x84, 0x8b, 0xa6, 0x73, 0x53, 0xc8, 0xf4, 0xf2, 0x1a, 0x06, 0xcb,
	0xdc, 0xf2, 0x7b, 0xaa, 0x7e, 0xe4, 0x86, 0xf0, 0x0a, 0x7a, 0x87, 0xff, 0xe1, 0xa1, 0x18, 0x8d,
	0x4e, 0x3d, 0x1b, 0xff, 0xbb, 0x72, 0xc5, 0x6e, 0xda, 0xba, 0xef, 0xc6, 0x1e, 0x5e, 0xfd, 0x0e,
	0x00, 0x00, 0xff, 0xff, 0x91, 0xea, 0x40, 0x69, 0x9e, 0x02, 0x00, 0x00,
}
