package client

import "gitee.com/LearingIt/go-aiot/aiotProto/aiot"

// 回调接口
type ClientCallBack interface {
	// 心跳响应
	OnHeartBeat(cli *Client, msg *aiot.Protocol)
	// 设备注册响应
	OnRegister(cli *Client, msg *aiot.Protocol) bool
	// 收到响应回调方法
	OnResponse(cli *Client, msg *aiot.Protocol)
	// 收到请求
	OnRequest(cli *Client, msg *aiot.Protocol)
	// 设备控制
	OnDeviceControl(cli *Client, msg *aiot.Protocol)
	// 数据下发
	OnDataReport(cli *Client, msg *aiot.Protocol)
	// 关闭连接回调方法
	OnClose(cli *Client)
}
