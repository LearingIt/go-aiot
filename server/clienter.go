package server

import (
	"encoding/json"
	"gitee.com/LearingIt/go-aiot/aiotProto/aiot"
	"gitee.com/LearingIt/go-aiot/client"
	"go.uber.org/zap"
)

// 实现client
type Clients struct {
}

// 实现OnHeartBeat
func (c *Clients) OnHeartBeat(cli *client.Client, msg *aiot.Protocol) {
	// 设备未注册不回心跳
	if !Srv.IsOnline(msg.SenderId) {
		// 记录日志
		cli.Logger.Error("Cluster have not registered", zap.Any("msg", msg))
		cli.Close()
		return
	}
	// 回复心跳
	msg.SenderId = Srv.serverId
	msg.ReqType = aiot.REQ_TYPE_RESPONSE
	msg.MsgProto = client.GetMsgProto(msg.MsgProto.MsgId)
	_ = cli.WriteBody(msg)
	// 上报到服务端
	go Srv.serverCallBack.OnHeartBeat(cli, msg)
	return
}

// 实现OnRegister
func (c *Clients) OnRegister(cli *client.Client, msg *aiot.Protocol) bool {
	Srv.RegisterDevice(cli, msg)
	isRegister := Srv.serverCallBack.OnRegister(cli, msg)
	if isRegister {
		// 注册回执
		registerData := aiot.DeviceRegister{
			ServerId:   Srv.serverId,
			ServerName: Srv.serverName,
			Ip:         Srv.serverIp,
		}
		registerByte, _ := json.Marshal(registerData)
		msgFeedBack := &aiot.Protocol{
			SenderId:   Srv.serverId,
			ReceiverId: msg.SenderId,
			MsgType:    aiot.MSG_TYPE_REGISTER,
			ReqType:    aiot.REQ_TYPE_RESPONSE,
			MsgProto:   client.GetMsgProto(msg.MsgProto.MsgId),
			Data:       registerByte,
		}
		_ = cli.WriteBody(msgFeedBack)
	} else {
		cli.Close()
		Srv.serverCallBack.OnClose(cli)
	}
	return isRegister
}

// 实现OnRequest
func (c *Clients) OnRequest(cli *client.Client, msg *aiot.Protocol) {
	go Srv.serverCallBack.OnRequest(cli, msg)
}

// 实现OnResponse
func (c *Clients) OnResponse(cli *client.Client, msg *aiot.Protocol) {
	go Srv.serverCallBack.OnResponse(cli, msg)
}

// 实现OnDataReport
func (c *Clients) OnDataReport(cli *client.Client, msg *aiot.Protocol) {
	go Srv.serverCallBack.OnDataReport(cli, msg)
}

// 实现OnDeviceControl
func (c *Clients) OnDeviceControl(cli *client.Client, msg *aiot.Protocol) {
	go Srv.serverCallBack.OnDeviceControl(cli, msg)
}

// 实现OnClose
func (c *Clients) OnClose(cli *client.Client) {
	Srv.serverCallBack.OnClose(cli)
	if Srv.IsOnline(cli.GetDeviceId()) {
		Srv.Remove(cli.GetDeviceId())
	}
	return
}
