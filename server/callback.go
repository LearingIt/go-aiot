package server

import (
	"gitee.com/LearingIt/go-aiot/aiotProto/aiot"
	"gitee.com/LearingIt/go-aiot/client"
)

// 回调接口
type ServerCallBack interface {
	// 收到心跳
	OnHeartBeat(c *client.Client, msg *aiot.Protocol)
	// 设备注册
	OnRegister(c *client.Client, msg *aiot.Protocol) bool
	// 收到请求
	OnRequest(c *client.Client, msg *aiot.Protocol)
	// 收到响应
	OnResponse(c *client.Client, msg *aiot.Protocol)
	// 数据上报
	OnDataReport(c *client.Client, msg *aiot.Protocol)
	// 设备控制
	OnDeviceControl(c *client.Client, msg *aiot.Protocol)
	// 通道关闭
	OnClose(c *client.Client)
}
