package server

import (
	"encoding/json"
	"errors"
	"gitee.com/LearingIt/go-aiot/aiotProto/aiot"
	"gitee.com/LearingIt/go-aiot/client"
	"gitee.com/LearingIt/go-aiot/util"
	uuid "github.com/satori/go.uuid"
	"go.uber.org/zap"
	"net"
	"net/http"
	"runtime"
	"strings"
	"sync"
	"time"
)

// 服务端结构体
type Server struct {
	// 监听地址，格式：0.0.0.0:7081
	addr string
	// 携程库
	waitGroup *util.WaitGroupWrapper
	// tcpListener
	tcpListener *net.TCPListener
	// tcpAddr
	tcpAddr *net.TCPAddr
	// 连接状态
	state State
	// 服务端设备ID
	serverId string
	// 服务器名称
	serverName string
	// 本机IP
	serverIp string
	// 设备信息写入锁
	deviceLock *sync.RWMutex
	// 所有设备ID列表 key: masterId
	Devices map[string]*client.Client
	// 回调接口
	serverCallBack ServerCallBack
	// logger
	Logger *zap.SugaredLogger
	// 守护进程间隔时间
	RestartTime time.Duration
	// 定时器
	Ticker *time.Ticker
}

// 连接状态
type State int32

// 连接状态
const (
	// 初始化
	StateInit State = iota
	// 已注册
	StateStarted
	// 已关闭
	StateClosed
)

// 全局服务
var Srv *Server

// 上锁
func (s *Server) Lock() {
	s.deviceLock.Lock()
}

// 解锁
func (s *Server) UnLock() {
	s.deviceLock.Unlock()
}

// 全局同步消息
type ReqChain struct {
	// 消息锁
	Lock *sync.Mutex
	// 消息channel
	Chain map[string]chan *aiot.Protocol
}

// 全局消息channel
var RequestChain *ReqChain

// 初始化服务
func NewServer(addr string, serverId string, serverName string, serverIp string, serverCallBack ServerCallBack, logger *zap.SugaredLogger, restartTime time.Duration) *Server {
	logger.Debug("New server...", zap.String("addr", addr))
	RequestChain = &ReqChain{
		Lock:  new(sync.Mutex),
		Chain: make(map[string]chan *aiot.Protocol),
	}
	return &Server{
		addr:           addr,
		waitGroup:      &util.WaitGroupWrapper{},
		serverId:       serverId,
		serverName:     serverName,
		deviceLock:     new(sync.RWMutex),
		Devices:        make(map[string]*client.Client),
		serverCallBack: serverCallBack,
		Logger:         logger,
		state:          StateInit,
		RestartTime:    restartTime,
		Ticker:         time.NewTicker(restartTime),
		serverIp:       serverIp,
	}
}

// 启动服务
func (s *Server) StartSrv() {
	go s.run()
	select {
	case <-s.Ticker.C:
		go s.run()
	}
}

func (s *Server) run() {
	s.Logger.Debug("Start server...", zap.String("addr", s.addr))
	if s.state == StateStarted {
		s.Logger.Debug("server has started...")
		return
	}
	// 错误
	var err error
	// tcpAddr
	s.tcpAddr, err = net.ResolveTCPAddr("tcp", s.addr)
	if err != nil {
		s.Logger.Error("Can not build tcp server for now", zap.String("addr", s.addr), zap.Error(err))
		return
	}

	// 监听
	s.tcpListener, err = net.ListenTCP("tcp", s.tcpAddr)
	if err != nil {
		s.Logger.Error("Can not listen tcp server for now", zap.String("addr", s.addr), zap.Error(err))
		return
	}

	// 收到连接
	s.Logger.Debug("Starting to listen addr", zap.String("addr", s.addr))
	s.SetStatus(StateStarted)
	s.waitGroup.Wrap(func() {
		for {
			// 获取连接
			clientConn, err := s.tcpListener.Accept()
			if err != nil {
				// 让出grouting
				if netErr, ok := err.(net.Error); ok && netErr.Temporary() {
					s.Logger.Error("Continue listening...", zap.String("addr", s.tcpListener.Addr().String()), zap.Error(err))
					runtime.Gosched()
					continue
				}

				// 不能使用已关闭的连接
				if !strings.Contains(err.Error(), "use of closed network connection") {
					s.Logger.Error("Can not use a closed network connection", zap.String("addr", s.addr), zap.Error(err))
				}
				break
			}

			// 处理连接
			s.Logger.Debug("Client connected", zap.String("RemoteAddr", clientConn.RemoteAddr().String()), zap.String("LocalAddr", clientConn.LocalAddr().String()))
			go s.Handler(clientConn)
		}
	})

	// wait
	s.waitGroup.Wait()
	s.Logger.Warn("Tcp server exit", zap.String("addr", s.addr))
	s.SetStatus(StateClosed)
	return
}

// 设置连接状态
func (s *Server) SetStatus(state State) {
	s.state = state
}

// 处理连接
func (s *Server) Handler(clientConn net.Conn) {
	s.Logger.Debug("Client connected...", zap.String("RemoteAddr", clientConn.RemoteAddr().String()), zap.String("LocalAddr", clientConn.LocalAddr().String()))

	// 临时ID
	tplClientId := uuid.NewV4().String()

	// 注册信息
	cliRegister := &aiot.DeviceRegister{}

	// 初始化连接
	cliCon := &Clients{}
	cli := client.NewClient(s.addr, tplClientId, cliRegister, cliCon, s.Logger, s.RestartTime)

	// 设置连接状态
	cli.SetState(client.StateConnected)
	cli.Conn = clientConn

	// 启用读写句柄
	cli.SetRWBuf()

	// wait
	cli.Wait()
	cli.Close()
}

// 处理设备注册
func (s *Server) RegisterDevice(cli *client.Client, msg *aiot.Protocol) {
	// 设置节点ID
	cli.SetDeviceId(msg.SenderId)
	s.Devices[msg.SenderId] = cli
}

// 是否注册
func (s *Server) IsOnline(deviceId string) bool {
	s.Lock()
	defer s.UnLock()
	if _, ok := s.Devices[deviceId]; ok {
		return true
	}
	return false
}

// 删除离线设备
func (s *Server) Remove(deviceId string) bool {
	s.Lock()
	defer s.UnLock()
	if _, ok := s.Devices[deviceId]; ok {
		delete(s.Devices, deviceId)
	}
	return false
}

// 错误结果拼装
func (s *Server) responseErr(msg string, reply *aiot.BusinessReply) error {
	var err error
	err = errors.New(msg)
	s.Logger.Error(err)
	reply.Success = false
	reply.Code = http.StatusBadRequest
	reply.Msg = msg
	return err
}

func (s *Server) formReq(receiverId string, req *aiot.Protocol) {
	// 发送者
	if req.SenderId == "" {
		req.SenderId = s.serverId
	}
	// 接收者
	if req.ReceiverId == "" {
		req.ReceiverId = receiverId
	}
	// 消息体
	if req.MsgProto == nil {
		req.MsgProto = client.GetMsgProto("")
	}
	// msgType
	if req.MsgType == aiot.MSG_TYPE_HEART_BEAT {
		req.MsgType = aiot.MSG_TYPE_BUSINESS
	}
}

// 同步发送请求
func (s *Server) SyncRequest(deviceId string, req *aiot.Protocol, reply *aiot.BusinessReply) error {
	var err error
	// 设备ID
	if deviceId == "" {
		return s.responseErr("设备ID为空", reply)
	}
	s.Lock()
	if _, ok := s.Devices[deviceId]; !ok {
		return s.responseErr("设备未注册", reply)
	}
	s.UnLock()
	// 整理参数
	s.formReq(deviceId, req)
	// 启用信息通道
	msgChain := make(chan *aiot.Protocol)
	RequestChain.Lock.Lock()
	RequestChain.Chain[req.MsgProto.MsgId] = msgChain
	RequestChain.Lock.Unlock()

	// 同步阻塞
	waiter := sync.WaitGroup{}
	waiter.Add(1)
	// 读取信道
	go func() {
		select {
		case res := <-msgChain:
			// 解码消息
			err = json.Unmarshal(res.Data, reply)
			s.Logger.Debug("aiot-callback-reply", reply)
			if err != nil {
				s.Logger.Error("Fail to Unmarshal business data", err, req, res)
			}
			// 关闭channel
			RequestChain.Lock.Lock()
			close(msgChain)
			delete(RequestChain.Chain, req.MsgProto.MsgId)
			RequestChain.Lock.Unlock()
			// 结束grouting
			s.Logger.Debug("success to get data", reply)
			waiter.Done()
			return
		case <-time.After(util.TcpWaitCallBackTimeOut):
			// 读取超时控制
			s.Logger.Error("Client does not response msg", req)
			// 关闭channel
			RequestChain.Lock.Lock()
			close(msgChain)
			delete(RequestChain.Chain, req.MsgProto.MsgId)
			RequestChain.Lock.Unlock()
			// 结束grouting
			err = errors.New("client does not response msg")
			waiter.Done()
			return
		}
	}()

	// 发送请求
	err = s.Devices[deviceId].WriteBody(req)
	waiter.Wait()
	if err != nil {
		s.Logger.Error("数据发送失败:", err)
		return errors.New("数据发送失败 err:" + err.Error())
	}
	return nil
}

// 异步发送请求
func (s *Server) ASyncRequest(deviceId string, req *aiot.Protocol) error {
	var err error
	// 设备ID
	if deviceId == "" {
		s.Logger.Error("设备ID为空")
		return errors.New("设备ID为空")
	}
	s.Lock()
	if _, ok := s.Devices[deviceId]; !ok {
		s.Logger.Error("设备未注册:", deviceId)
		return errors.New("设备未注册")
	}
	s.UnLock()
	// 整理参数
	s.formReq(deviceId, req)
	// 发送请求
	err = s.Devices[deviceId].WriteBody(req)
	if err != nil {
		s.Logger.Error("数据发送失败:", err)
		return errors.New("数据发送失败 err:" + err.Error())
	}
	return nil
}
