package util

import "time"

// 时长控制
const (
	// 最大消息长度
	DefaultBufferSize = 512 * 1024
	// 心跳间隔
	DefaultHeartbeatInterval = 15 * time.Second
	// 读取数据超时时间
	DefaultReaderTimeOut = 60 * time.Second
	// 服务守护间隔时长
	TcpWaitCallBackTimeOut = 15 * time.Second
)
