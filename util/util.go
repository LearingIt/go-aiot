package util

import (
	"sync"
)

// 协程等待结构体
type WaitGroupWrapper struct {
	sync.WaitGroup
}

// 异步启动
func (w *WaitGroupWrapper) Wrap(b func()) {
	w.Add(1)
	go func() {
		b()
		w.Done()
	}()
}
