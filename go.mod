module gitee.com/LearingIt/go-aiot

go 1.14

require (
	github.com/docker/go-metrics v0.0.1 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/micro/go-micro v1.18.0
	github.com/satori/go.uuid v1.2.0
	go.uber.org/zap v1.21.0
)
